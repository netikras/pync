# pync

a dead simple implementation of NC in python.

Purpose: docker images often do not have basic shell utils installed but they DO have python. nc.py is useful to debug network issues in containers

Usage:

./nc.py -c remote_host 3544

./nc.py -s localhost 8080

./nc .py -c database_address 3306 -i

./nc .py -s localhost 13306 -i


-s -- server

-c -- client

-i -- interactive
