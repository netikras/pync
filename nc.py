#!/usr/bin/env python

import socket
import select

from sys import stdin, stdout, stderr, exit, argv
from os import isatty

help_msg = "Usage: nc.py [-c|-s] <address> <port> [-i]"

class Netcat:
    def __init__(self):
        self.s = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
        self.echo = False

    def listen(self, address, port):
        self.s.setsockopt(socket.SOL_SOCKET, socket.SO_REUSEADDR, 1)
        self.s.bind((address,port))
        self.s.listen(16)
        c, addr = self.s.accept()
        self.s = c
        return addr

    def connect(self, address, port):
        self.s.connect((address, port))

    def send(self, data):
        self.s.sendall(data.encode())

    def read(self, count, timeout=10):
        _r,_,_ = select.select([self.s], [], [], timeout)
        if not _r:
            return b''
        data = self.s.recv(count)
        if self.echo:
            stdout.write(data)
        return data

    def readAll(self, timeout=10):
        buff = b''
        while True:
            data = self.read(4096, timeout)
            if not data:
                break
            buff += data
        return buff

    def interact(self):
        while True:
            _r,_w,_x = select.select([self.s, stdin], [], [])
            for readable in _r:
                if readable == stdin:
                    data = stdin.readline()
                    self.send(data)
                elif readable == self.s:
                    data = self.read(256)
                    if not data:
                        self.close()
                        break
                    stdout.write(data)

    def close(self):
        self.s.close()

def die(code, msg):
    stderr.write(msg + "\n")
    exit(code)

def main(argv):
    is_piped = not isatty(stdin.fileno())
    msg = None

    if is_piped:
        msg = stdin.read()

    nc = Netcat()

    if len(argv) == 0:
        die(1, help_msg)
    elif argv[0] == '-c':
        nc.connect(argv[1], int(argv[2]))
    elif argv[0] == '-s':
        nc.listen(argv[1], int(argv[2]))
    else:
        die(1, help_msg)

    print("Connected to " + argv[1] + ":" + argv[2])
    if msg:
        nc.send(msg)

    if len(argv) == 4 and argv[3] == '-i':
        nc.interact()
    else:
        nc.echo = True
        nc.readAll(9999)
    nc.close()


if __name__ == "__main__":
    main(argv[1:])

